from datetime import datetime

import dateparser
from PyQt5.QtCore import Qt

sessionProperties = ["Datum", "Name", "Dauer / Minuten", "Einheiten",
                     "Hausbesuch", "Strecke / km"]


class Session:

    def __init__(self):
        self.__name = "Max Mustermann"
        self.__date = datetime.today()
        self.__duration = 45
        self.__units = 1
        self.__home = False
        self.__distance = 0.

    def toDict(self):
        return {"name": self.__name, "date": self.__date.strftime("%d.%m.%Y"),
                "duration": self.__duration, "units": self.units,
                "home": self.__home, "distance": self.__distance}

    def fromDict(self, d):
        if "name" in d:
            self.name = d["name"]
        if "date" in d:
            self.date = datetime.strptime(d["date"], "%d.%m.%Y")
        if "duration" in d:
            self.duration = d["duration"]
        if "units" in d:
            self.units = d["units"]
        if "home" in d:
            self.__home = d["home"]
        if "distance" in d:
            self.__distance = d["distance"]

    def filter(self, key, value):
        if key == "Name":
            return value == self.name
        elif key == "Monat":
            return value == self.date.strftime("%B %Y")
        elif key == "Jahr":
            return value == self.date.strftime("%Y")
        elif key == "Datum":
            return value == self.dateFormatted
        elif key == "Wochentag":
            return value == self.date.strftime("%A")
        elif key == "Dauer / Minuten":
            if isinstance(value, int):
                return value == self.duration
            else:
                return value == str(self.duration)
        elif key == "Hausbesuch":
            return value == self.homeFormatted
        return True

    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self, value):
        self.setName(value)

    def setName(self, value):
        try:
            self.__name = str(value)
        except Exception:
            return False
        return True

    @property
    def date(self):
        return self.__date

    @property
    def dateFormatted(self):
        return self.__date.strftime("%d.%m.%Y")

    @property
    def dateFormattedWeekday(self):
        return self.__date.strftime("%a, %d.%m.%Y")

    @date.setter
    def date(self, value):
        self.setDate(value)

    def setDate(self, value):
        if isinstance(value, datetime):
            self.__date = value
            return True
        else:
            try:
                date = dateparser.parse(str(value), languages=["de"])
                if date is not None:
                    self.__date = date
                    return True
                return False
            except Exception:
                return False

    @property
    def duration(self):
        return self.__duration

    @duration.setter
    def duration(self, value):
        self.setDuration(value)

    def setDuration(self, value):
        try:
            self.__duration = int(value)
        except Exception:
            return False
        return True

    @property
    def units(self):
        return self.__units

    @units.setter
    def units(self, value):
        self.setUnits(value)

    def setUnits(self, value):
        try:
            self.__units = float(value)
        except Exception:
            return False
        return True

    @property
    def home(self):
        return self.__home

    @property
    def homeFormatted(self):
        if self.__home:
            return "Ja"
        return "Nein"

    @home.setter
    def home(self, value):
        self.setHome(value)

    def setHome(self, value):
        if isinstance(value, bool):
            self.__home = value
            return True
        else:
            try:
                if str(value).lower() == "ja":
                    self.__home = True
                elif str(value).lower() == "nein":
                    self.__home = False
                else:
                    return False
                return True
            except Exception:
                return False

    @property
    def distance(self):
        return self.__distance

    @distance.setter
    def distance(self, value):
        self.setDistance(value)

    def setDistance(self, value):
        try:
            self.__distance = float(value)
        except Exception:
            return False
        return True

    def data(self, index, role=Qt.EditRole):
        if isinstance(index, int):
            if index == 0:
                if role == Qt.DisplayRole:
                    return self.dateFormattedWeekday
                else:
                    return self.dateFormatted
            elif index == 1:
                return self.name
            elif index == 2:
                return self.duration
            elif index == 3:
                return self.units
            elif index == 4:
                return self.homeFormatted
            elif index == 5:
                return self.distance
        elif isinstance(index, str):
            if index == "Name":
                return self.name
            elif index == "Datum":
                return self.dateFormatted
            elif index == "Dauer / Minuten":
                return self.duration
            elif index == "Einheiten":
                return self.units
            elif index == "Hausbesuch":
                return self.homeFormatted
            elif index == "Strecke / km":
                return self.distance

    def setData(self, index, value):
        if isinstance(index, int):
            if index == 0:
                return self.setDate(value)
            elif index == 1:
                return self.setName(value)
            elif index == 2:
                return self.setDuration(value)
            elif index == 3:
                return self.setUnits(value)
            elif index == 4:
                return self.setHome(value)
            elif index == 5:
                return self.setDistance(value)
        elif isinstance(index, str):
            if index == "Name":
                return self.setName(value)
            elif index == "Datum":
                return self.setDate(value)
            elif index == "Dauer / Minuten":
                return self.setDuration(value)
            elif index == "Einheiten":
                return self.setUnits(value)
            elif index == "Hausbesuch":
                return self.setHome(value)
            elif index == "Strecke / km":
                return self.setDistance(value)
        return False
