from PyQt5.QtWidgets import *

lastChoice = 0


class DuplicateDialog(QDialog):

    def __init__(self, parent=None, callback=None):
        super().__init__(parent=parent)
        self.__callback = callback
        self.__layout = QVBoxLayout()
        self.setLayout(self.__layout)
        self.label = QLabel(
            "Bitte wählen Sie das Datum der duplizierten Einträge aus:")
        self.rdbSameDay = QRadioButton("Gleicher Tag")
        self.rdbNextDay = QRadioButton("Ein Tag später")
        self.rdbNextWeek = QRadioButton("Eine Woche später")
        self.__layout.addWidget(self.label)
        self.__layout.addWidget(self.rdbSameDay)
        self.__layout.addWidget(self.rdbNextDay)
        self.__layout.addWidget(self.rdbNextWeek)

        self.__rButtons = [self.rdbSameDay, self.rdbNextDay, self.rdbNextWeek]
        self.__rButtons[lastChoice].setChecked(True)

        for b in self.__rButtons:
            b.clicked.connect(self.__selectionChanged)

        self.btnOK = QPushButton("Duplizieren")
        self.btnAbort = QPushButton("Abbrechen")
        self.buttonBox = QHBoxLayout()
        self.buttonBox.addWidget(self.btnOK)
        self.buttonBox.addWidget(self.btnAbort)
        self.btnOK.pressed.connect(self.__ok)
        self.btnAbort.pressed.connect(self.__abort)
        self.__layout.addLayout(self.buttonBox)

    def __selectionChanged(self, source: QRadioButton):
        global lastChoice
        for i, b in enumerate(self.__rButtons):
            if b.isChecked():
                lastChoice = i
                break

    def __ok(self):
        self.__callback(lastChoice)
        self.__abort()

    def __abort(self):
        self.setParent(None)
        self.close()
