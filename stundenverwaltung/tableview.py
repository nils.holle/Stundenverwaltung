from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *


class TableView(QTableView):

    def __init__(self, methods, parent=None):
        super().__init__(parent)
        self.__methods = methods
        self.setSelectionBehavior(QTableView.SelectRows)
        self.setEditTriggers(QTableView.DoubleClicked)
        self.setContextMenuPolicy(Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.__showContextMenu)

    def __showContextMenu(self, point):
        menu = QMenu()
        addAction = menu.addAction("Stunde hinzufügen")
        duplicateAction = menu.addAction("Ausgewählte duplizieren")
        deleteAction = menu.addAction("Ausgewählte löschen")
        addAction.triggered.connect(self.__methods["add"])
        duplicateAction.triggered.connect(self.__methods["duplicate"])
        deleteAction.triggered.connect(self.__methods["delete"])
        menu.exec(QCursor.pos())
