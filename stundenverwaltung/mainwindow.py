import calendar
import json
import locale
import os
from copy import deepcopy
from datetime import timedelta

import numpy as np
from appdirs import user_config_dir
from PyQt5 import QtCore, QtGui, QtWidgets
from reportlab.lib import colors
from reportlab.lib.pagesizes import A4
from reportlab.lib.styles import ParagraphStyle
from reportlab.platypus import Paragraph, SimpleDocTemplate, Table, TableStyle

from stundenverwaltung.duplicate import DuplicateDialog
from stundenverwaltung.session import Session, sessionProperties
from stundenverwaltung.tableheader import TableHeader
from stundenverwaltung.tablemodel import TableModel
from stundenverwaltung.tableview import TableView

filters = ["Name", "Monat", "Jahr", "Datum", "Wochentag",
           "Dauer / Minuten", "Hausbesuch"]


class MainWindow(QtWidgets.QMainWindow):

    def __init__(self):
        super().__init__()
        locale.setlocale(locale.LC_ALL, "de_DE.utf8")
        currentDir = os.path.dirname(__file__)
        self.__sessions, self.__currentSessions = [], []

        # dicts for sorting
        self.__months = {v: k for k, v in enumerate(calendar.month_name)}
        self.__weekDays = {v: k for k, v in enumerate(calendar.day_name)}

        # menu bar
        self.setMenuBar(QtWidgets.QMenuBar(self))

        self.fileMenu = QtWidgets.QMenu("Datei", self.menuBar())
        self.mNewAction = self.fileMenu.addAction(
            QtGui.QIcon(os.path.join(currentDir, "icons/add-3.svg")),
            "Neu")
        self.mOpenAction = self.fileMenu.addAction(
            QtGui.QIcon(os.path.join(currentDir, "icons/folder.svg")),
            "Öffnen")
        self.mSaveAction = self.fileMenu.addAction(
            QtGui.QIcon(os.path.join(currentDir, "icons/save.svg")),
            "Speichern")
        self.mSaveAsAction = self.fileMenu.addAction("Speichern unter...")
        self.mSaveCopyAction = self.fileMenu.addAction(
            QtGui.QIcon(os.path.join(currentDir, "icons/copy.svg")),
            "Kopie speichern unter...")
        self.mNewAction.triggered.connect(self.new)
        self.mOpenAction.triggered.connect(self.open)
        self.mSaveAction.triggered.connect(self.save)
        self.mSaveAsAction.triggered.connect(self.saveAs)
        self.mSaveCopyAction.triggered.connect(self.saveCopy)
        self.menuBar().addMenu(self.fileMenu)

        self.exportMenu = QtWidgets.QMenu("Exportieren", self.menuBar())
        self.exportTableAction = self.exportMenu.addAction(
            QtGui.QIcon(os.path.join(currentDir, "icons/paper-plane-1.svg")),
            "Tabelle")
        self.exportTableAction.triggered.connect(self.exportTable)
        self.menuBar().addMenu(self.exportMenu)

        # tool bar
        self.toolBar = QtWidgets.QToolBar(self)
        self.toolBar.setMovable(False)
        self.tbNewAction = self.toolBar.addAction(
            QtGui.QIcon(os.path.join(currentDir, "icons/add-3.svg")),
            "Neue Datei")
        self.tbOpenAction = self.toolBar.addAction(
            QtGui.QIcon(os.path.join(currentDir, "icons/folder.svg")),
            "Datei öffnen")
        self.tbUndoAction = self.toolBar.addAction(
            "Undo"
        )
        self.tbRedoAction = self.toolBar.addAction(
            "Redo"
        )
        self.tbSaveAction = self.toolBar.addAction(
            QtGui.QIcon(os.path.join(currentDir, "icons/save.svg")),
            "Datei speichern")
        self.tbSaveCopyAction = self.toolBar.addAction(
            QtGui.QIcon(os.path.join(currentDir, "icons/copy.svg")),
            "Kopie speichern unter...")
        self.tbExportAction = self.toolBar.addAction(
            QtGui.QIcon(os.path.join(currentDir, "icons/paper-plane-1.svg")),
            "Tabelle exportieren")
        self.tbNewAction.triggered.connect(self.new)
        self.tbOpenAction.triggered.connect(self.open)
        self.tbSaveAction.triggered.connect(self.save)
        self.tbSaveCopyAction.triggered.connect(self.saveCopy)
        self.tbExportAction.triggered.connect(self.exportTable)
        self.addToolBar(self.toolBar)

        # layout
        self.widget = QtWidgets.QSplitter(self)
        self.widget.setOrientation(QtCore.Qt.Horizontal)
        self.setCentralWidget(self.widget)

        self.vLayoutLWidget = QtWidgets.QWidget(self.widget)
        self.vLayoutRWidget = QtWidgets.QWidget(self.widget)
        self.vLayoutL = QtWidgets.QVBoxLayout()
        self.vLayoutR = QtWidgets.QVBoxLayout()
        self.vLayoutLWidget.setLayout(self.vLayoutL)
        self.vLayoutRWidget.setLayout(self.vLayoutR)
        self.widget.addWidget(self.vLayoutLWidget)
        self.widget.addWidget(self.vLayoutRWidget)

        # left area (drop down + list widget + delete filter button)
        self.dropDown = QtWidgets.QComboBox(self.widget)
        self.listWidget = QtWidgets.QListWidget(self.widget)
        self.listWidget.currentItemChanged.connect(
            self.__listWidgetItemChanged)
        self.deleteFilterBtn = QtWidgets.QPushButton("Filter löschen",
                                                     self.widget)
        self.deleteFilterBtn.pressed.connect(self.__deleteFilter)
        self.vLayoutL.addWidget(self.dropDown)
        self.vLayoutL.addWidget(self.listWidget)
        self.vLayoutL.addWidget(self.deleteFilterBtn)

        for key in filters:
            self.dropDown.addItem(key)
        self.dropDown.currentIndexChanged.connect(self.__dropDownIndexChanged)

        # right area (table view + buttons)
        self.tableView = TableView({"add": self.__addSession,
                                    "duplicate": self.__duplicateSessions,
                                    "delete": self.__deleteSessions},
                                   self.widget)
        self.tableView.setModel(TableModel())
        self.tableView.doubleClicked.connect(
            self.tableView.model().doubleClicked)
        self.tableView.model().layoutChanged.connect(self.__dataChanged)
        self.tableView.model().dataChanged.connect(self.__dataChanged)

        self.headerView = TableHeader(QtCore.Qt.Horizontal, self.tableView)
        self.headerView.clicked.connect(self.sortByColumn)
        self.tableView.setHorizontalHeader(self.headerView)
        for i in range(self.tableView.horizontalHeader().count()):
            self.headerView.setSectionResizeMode(
                i, QtWidgets.QHeaderView.Stretch)
        self.vLayoutR.addWidget(self.tableView)

        self.btnLayout = QtWidgets.QHBoxLayout()
        self.btnLayoutWidget = QtWidgets.QWidget(self.widget)
        self.btnLayoutWidget.setLayout(self.btnLayout)
        self.vLayoutR.addWidget(self.btnLayoutWidget)
        self.sumLabel = QtWidgets.QLabel(self.widget)
        self.btnLayout.addWidget(self.sumLabel)
        self.btnLayout.addSpacerItem(
            QtWidgets.QSpacerItem(10, 10, QtWidgets.QSizePolicy.Expanding))
        self.addBtn = QtWidgets.QPushButton("Stunde hinzufügen", self.widget)
        self.addBtn.pressed.connect(self.__addSession)
        self.btnLayout.addWidget(self.addBtn)
        self.duplicateBtn = QtWidgets.QPushButton("Duplizieren", self.widget)
        self.duplicateBtn.pressed.connect(self.__duplicateSessions)
        self.btnLayout.addWidget(self.duplicateBtn)
        self.deleteBtn = QtWidgets.QPushButton("Löschen", self.widget)
        self.deleteBtn.pressed.connect(self.__deleteSessions)
        self.btnLayout.addWidget(self.deleteBtn)

        self.widget.setStretchFactor(0, 0)
        self.widget.setStretchFactor(1, 1)

        self.resize(1250, 750)
        self.showMaximized()

        self.__lastSortIndex, self.__reverse = 0, False
        self.__updateSums()

        self.__filePath, self.__dirty = "", False
        self.__shortcuts = {}
        self.addShortcut("Ctrl+s", self.save)
        self.addShortcut("Ctrl+n", self.__addSession)
        self.addShortcut("Ctrl+d", self.__duplicateSessions)
        self.addShortcut(QtCore.Qt.Key_Delete, self.__deleteSessions)
        self.addShortcut(QtCore.Qt.Key_Backspace, self.__deleteSessions)

        self.tbUndoAction.triggered.connect(self.tableView.model().undo)
        self.tbRedoAction.triggered.connect(self.tableView.model().redo)

        # using Windows, user_config_dir() is already the appropriate directory
        if os.name == "nt":
            self.__configDir = user_config_dir()
        else:
            self.__configDir = user_config_dir("Stundenverwaltung")
        self.__loadLastFile()

    def addShortcut(self, sequence, target):
        sc = QtWidgets.QShortcut(QtGui.QKeySequence(sequence), self)
        sc.activated.connect(target)
        self.__shortcuts[sequence] = sc

    """
    ===========================================================================
        Misc
    """

    @property
    def dirty(self):
        return self.__dirty

    @dirty.setter
    def dirty(self, value):
        self.__dirty = value
        self.tbSaveAction.setEnabled(self.__dirty)
        self.updateWindowTitle()

    def __getSelectedRows(self):
        rows = []
        for ind in self.tableView.selectionModel().selectedRows(0):
            rows.append(ind.row())
        return rows

    def __dataChanged(self, *args):
        self.__updateSums()
        self.__updateFilterList()
        self.dirty = True

    def updateWindowTitle(self):
        title = "Stundenverwaltung"
        if self.__filePath != "":
            title += " - " + self.__filePath
        if self.dirty:
            title += "*"
        self.setWindowTitle(title)

    """
    ===========================================================================
        Add, duplicate, delete sessions
    """

    def __addSession(self):
        s = Session()
        self.__sessions.append(s)
        self.__currentSessions.append(s)
        self.tableView.model().setSessionList(self.__currentSessions)

    def __duplicateSessions(self):
        d = DuplicateDialog(self, self.__duplicateSessionsCallback)
        d.show()

    def __duplicateSessionsCallback(self, choice):
        indices, itemsToDuplicate = [], []
        for i in self.__getSelectedRows():
            indices.append(i)
            itemsToDuplicate.append(deepcopy(self.__currentSessions[i]))

        for i, item in enumerate(itemsToDuplicate):
            if choice == 1:
                item.date += timedelta(days=1)
            elif choice == 2:
                item.date += timedelta(weeks=1)
            self.__sessions.append(item)
            self.__currentSessions.insert(indices[i] + len(indices), item)

        self.tableView.model().setSessionList(self.__currentSessions)

    def __deleteSessions(self):
        reply = QtWidgets.QMessageBox.question(
            self, "Löschen bestätigen",
            "Möchten Sie die ausgewählten Stunden wirklich löschen?",
            QtWidgets.QMessageBox.No, QtWidgets.QMessageBox.Yes)

        if reply == QtWidgets.QMessageBox.Yes:
            nDeleted = 0

            for i in self.__getSelectedRows():
                s = self.__currentSessions.pop(i - nDeleted)
                del self.__sessions[self.__sessions.index(s)]
                nDeleted += 1
            self.tableView.model().setSessionList(self.__currentSessions)

    """
    ===========================================================================
        Status text
    """

    def __updateSums(self):
        self.sumLabel.setText(self.__getSumText())

    def __getSumText(self):
        text = "%d Stunde" % (len(self.__currentSessions))
        if len(self.__currentSessions) == 0 or len(self.__currentSessions) > 1:
            text += "n"

        nUnits = sum([s.units for s in self.__currentSessions])
        text += ", %.1f Einheit" % (nUnits)
        if nUnits == 0 or nUnits > 1:
            text += "en"

        nHome = np.count_nonzero([s.home for s in self.__currentSessions])
        text += ", %d Hausbesuch" % (nHome)
        if nHome == 0 or nHome > 1:
            text += "e"

        distance = sum([s.distance for s in self.__currentSessions])
        text += ", %.1f km" % (distance)

        nExtra = np.count_nonzero([s.home and s.distance >= 6 for s in
                                   self.__currentSessions])
        text += "; %d Hausbesuch" % (nExtra)
        if nExtra == 0 or nExtra > 1:
            text += "e"
        text += " mit mind. 3 km Entfernung"
        return text

    """
    ===========================================================================
        Sort sessions
    """

    def sortByColumn(self, index):
        data = [(s, s.data(index)) for s in self.__currentSessions]
        if len(data) > 0:
            data = sorted(data, key=lambda x: x[1],
                          reverse=(index == self.__lastSortIndex and not
                                   self.__reverse))
            self.__currentSessions = [s[0] for s in data]
            self.tableView.model().setSessionList(self.__currentSessions)
            self.__lastSortIndex, self.__reverse = index, not self.__reverse

    """
    ===========================================================================
        Session filtering
    """

    def __dropDownIndexChanged(self, index):
        self.__updateFilterList()

    def __listWidgetItemChanged(self, item):
        self.__filter()

    def __updateFilterList(self):
        index = self.dropDown.currentIndex()
        self.listWidget.clear()
        self.listWidget.addItems(self.__getValueList(filters[index]))

    def __getValueList(self, key):
        values = []

        for s in self.__sessions:
            if key == "Name" and s.name not in values:
                values.append(s.name)
            elif (key == "Monat" or key == "Jahr" or
                  key == "Datum" or key == "Wochentag"):
                d = s.date
                if key == "Monat" and d.strftime("%B %Y") not in values:
                    values.append(d.strftime("%B %Y"))
                elif key == "Jahr" and d.strftime("%Y") not in values:
                    values.append(d.strftime("%Y"))
                elif (key == "Datum" and s.dateFormatted not in values):
                    values.append(s.dateFormatted)
                elif key == "Wochentag" and d.strftime("%A") not in values:
                    values.append(d.strftime("%A"))
            elif key == "Dauer / Minuten" and str(s.duration) not in values:
                values.append(str(s.duration))
            elif key == "Hausbesuch" and s.homeFormatted not in values:
                values.append(s.homeFormatted)

        if key == "Monat":
            return sorted(
                values,
                key=lambda v: (v[-4:], self.__months[v[:-5]])
            )
        elif key == "Wochentag":
            return sorted(values, key=lambda v: self.__weekDays[v])
        return sorted(values)

    def __filter(self):
        key = filters[self.dropDown.currentIndex()]
        item = self.listWidget.currentItem()
        if item is not None:
            value = item.text()
            self.__currentSessions = []
            for s in self.__sessions:
                if s.filter(key, value):
                    self.__currentSessions.append(s)
            self.tableView.model().setSessionList(self.__currentSessions)

    def __deleteFilter(self):
        self.__currentSessions = self.__sessions
        self.tableView.model().setSessionList(self.__currentSessions)

    """
    ===========================================================================
        New / open / save file
    """

    def new(self):
        result = self.__checkDirty()
        if result:
            self.__sessions, self.__currentSessions = [], []
            self.__filePath = ""
            self.tableView.model().setSessionList(self.__currentSessions)
            self.dirty = False

    def open(self):
        result = self.__checkDirty()
        if result:
            path = QtWidgets.QFileDialog.getOpenFileName(
                self, filter="*.sv")[0]
            if path != "":
                self.__load(path)

    def save(self):
        if self.__filePath != "":
            self.__save(self.__filePath)
        else:
            self.saveAs()

    def saveAs(self):
        path = QtWidgets.QFileDialog.getSaveFileName(self, filter="*.sv")[0]
        if path != "":
            self.__filePath = path
            self.save()

    def saveCopy(self):
        path = QtWidgets.QFileDialog.getSaveFileName(self, filter="*.sv")[0]
        if path != "":
            self.__save(path)

    def __save(self, path):
        saveList = [s.toDict() for s in self.__sessions]
        saveList.append(self.__lastSortIndex)
        saveList.append(self.__reverse)
        with open(path, "w") as f:
            f.write(json.dumps(saveList))
        self.__saveLastFile()
        self.dirty = False

    def __load(self, path):
        with open(path, "r") as f:
            saveList = json.loads(f.read())

        sortIndex, reverse = None, None
        for s in saveList:
            if isinstance(s, bool):
                reverse = s
            elif isinstance(s, int):
                sortIndex = s

        self.__sessions, self.__currentSessions = [], []
        for d in saveList:
            if isinstance(d, dict):
                s = Session()
                s.fromDict(d)
                self.__sessions.append(s)
                self.__currentSessions.append(s)
        self.tableView.model().setSessionList(self.__currentSessions)
        self.__filePath = path
        self.__saveLastFile()

        if reverse is not None:
            self.__lastSortIndex = sortIndex
            self.__reverse = not reverse
        if sortIndex is not None:
            self.sortByColumn(sortIndex)

        self.dirty = False

    def __saveLastFile(self):
        if not os.path.isdir(self.__configDir):
            os.mkdir(self.__configDir)

        with open(self.__configDir + "/config.json", "w") as f:
            json.dump(self.__filePath, f)

    def __loadLastFile(self):
        if os.path.isfile(self.__configDir + "/config.json"):
            with open(self.__configDir + "/config.json", "r") as f:
                path = json.load(f)
            if os.path.isfile(path):
                self.__load(path)

    def __checkDirty(self):
        if self.dirty:
            reply = QtWidgets.QMessageBox.question(
                self, "Stundenverwaltung schließen",
                ("Möchten Sie vor dem Schließen speichern? " +
                 "Alle Änderungen gehen sonst verloren."),
                QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No |
                QtWidgets.QMessageBox.Abort,
                QtWidgets.QMessageBox.Yes)

            if reply == QtWidgets.QMessageBox.Yes:
                self.save()

            if reply == QtWidgets.QMessageBox.Abort:
                return False
        return True

    """
    ===========================================================================
        PDF export
    """

    def __currentSessionsToTable(self):
        table = [sessionProperties]
        for i, s in enumerate(self.__currentSessions):
            st = []
            if (i == 0 or s.date != self.__currentSessions[i - 1].date):
                st.append(s.dateFormattedWeekday)
            else:
                st.append("")
            st.append(s.name)
            st.append(str(s.duration))
            st.append(str(s.units))
            st.append(s.homeFormatted)
            st.append("%.1f" % (s.distance))
            table.append(st)
        return table

    def exportTable(self):
        path = QtWidgets.QFileDialog.getSaveFileName(self, filter="*.pdf")[0]

        if path != "":
            doc = SimpleDocTemplate(path, pagesize=A4)

            # container for the 'Flowable' objects
            text = "Gesamt: " + self.__getSumText()
            elements = [Paragraph(text, ParagraphStyle("Test"))]

            data = self.__currentSessionsToTable()
            t = Table(data, colWidths=[120, 140, 75, 50, 70, 60])
            elements.append(t)
            # write the document to disk
            doc.build(elements)

    """
    ===========================================================================
        Check if dirty on close
    """

    def closeEvent(self, event):
        result = self.__checkDirty()
        if result:
            event.accept()
        else:
            event.ignore()
