from PyQt5 import QtCore, QtWidgets

from stundenverwaltung.session import sessionProperties


class TableModel(QtCore.QAbstractTableModel):

    def __init__(self):
        super().__init__()
        self.__sessions = []
        self.__undoStack = QtWidgets.QUndoStack(self)

    def data(self, index, role=QtCore.Qt.DisplayRole):
        if index.isValid():
            if role == QtCore.Qt.DisplayRole or role == QtCore.Qt.EditRole:
                row, col = index.row(), index.column()
                if row < len(self.__sessions):
                    return QtCore.QVariant(
                        str(self.__sessions[row].data(col, role)))
        return QtCore.QVariant()

    def setData(self, index, value, role=QtCore.Qt.EditRole, store=True):
        result = False
        if index.isValid():
            if role == QtCore.Qt.EditRole:
                row, col = index.row(), index.column()
                oldData = str(self.__sessions[row].data(col, role))
                result = self.__sessions[row].setData(col, value)
        if result:
            self.dataChanged.emit(index, index)
            if store:
                undo = ChangeValueCommand(
                    self, index, oldData,
                    str(self.__sessions[row].data(col, role))
                )
                self.__undoStack.push(undo)
        return result

    def undo(self):
        self.__undoStack.undo()

    def redo(self):
        self.__undoStack.redo()

    def setSessionList(self, l):
        self.__sessions = l
        self.layoutChanged.emit()

    def rowCount(self, parent=None):
        return len(self.__sessions)

    def columnCount(self, parent=None):
        return len(sessionProperties)

    def headerData(self, section, orientation, role=QtCore.Qt.DisplayRole):
        if role == QtCore.Qt.DisplayRole:
            if orientation == QtCore.Qt.Horizontal:
                return QtCore.QVariant(sessionProperties[section])
            return super().headerData(section, orientation, role)
        return QtCore.QVariant()

    def flags(self, index):
        if sessionProperties[index.column()] == "Hausbesuch":
            return super().flags(index)
        return super().flags(index) | QtCore.Qt.ItemIsEditable

    def doubleClicked(self, index):
        if sessionProperties[index.column()] == "Hausbesuch":
            row = index.row()
            self.__sessions[row].home = not self.__sessions[row].home
            self.dataChanged.emit(index, index)


class ChangeValueCommand(QtWidgets.QUndoCommand):

    def __init__(self, model: TableModel, index: QtCore.QModelIndex,
                 oldValue: QtCore.QVariant, newValue: QtCore.QVariant):
        super().__init__()
        self.__model, self.__index = model, index
        self.__oldValue, self.__newValue = oldValue, newValue

    def undo(self):
        self.__model.setData(self.__index, self.__oldValue,
                             role=QtCore.Qt.EditRole, store=False)

    def redo(self):
        self.__model.setData(self.__index, self.__newValue,
                             role=QtCore.Qt.EditRole, store=False)
