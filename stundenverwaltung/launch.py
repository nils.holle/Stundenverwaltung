import sys

from PyQt5 import QtWidgets

from stundenverwaltung.mainwindow import MainWindow


def main():
    """ Create Application and MainWindow """
    global app, mainWindow
    app = QtWidgets.QApplication(sys.argv)
    mainWindow = MainWindow()
    return app.exec_()

if __name__ == "__main__":
    sys.exit(main())
