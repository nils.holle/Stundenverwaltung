from PyQt5.QtCore import Qt, pyqtSignal
from PyQt5.QtWidgets import QHeaderView


class TableHeader(QHeaderView):

    clicked = pyqtSignal(object)

    def mouseReleaseEvent(self, event):
        if event.button() == Qt.LeftButton:
            index = self.logicalIndexAt(event.pos())
            self.clicked.emit(index)
        return super().mouseReleaseEvent(event)
